var exec = require('child_process').exec;
var cmd = 'ln -sf ../lib/Data/ node_modules/Data && ln -sf ../lib/_config/ node_modules/config';

console.log('Initializing project...');

exec(cmd, function(err, stdout, stderr) {
	if (!err) {
		console.log(' - Initialization complete.');
	} else {
		throw new Error(err);
	}
});
