var config = require('config');
var express = require('express');
var should = require('should');
var request = require('request');

process.env.PORT = 3333;

var server = require('../index.js');

describe('GET by ID', function () {
	it('should return an object with that ID', function(done) {
		request.get('http://localhost:3333/api/test/1', function(err, res, body) {
			var result = JSON.parse(body);
			result.id.should.equal('1');
			done();
		});
	});
});

describe('GET a property of a record by ID and property', function(done) {
	it('should return a specific property of a specific object', function(done) {
		request.get('http://localhost:3333/api/test/1/location', function(err, res, body) {
			var result = JSON.parse(body);
			result.location.city.should.equal('Boston');
			done();
		});
	});
})

describe('GET by key-value pair', function () {
	it('should return objects that match that key-value pair', function(done) {
		request.get('http://localhost:3333/api/test/test/true', function(err, res, body) {
			var results = JSON.parse(body);
			results.length.should.be.above(1);
			results.forEach(function(result) {
				result.test.should.equal(true);
			});
			done();
		});
	});
});

describe('GET any record nearly matching a given input', function () {
	it('should return objects that nearly match a given input', function(done) {
		request.get('http://localhost:3333/api/test/type/~cus', function(err, res, body) {
			var results = JSON.parse(body);
			results.length.should.equal(1);
			results[0].type.should.equal('customer');
			done();
		});
	});
});