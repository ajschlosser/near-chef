var config = require('config').api;
var Data = require('Data');
var r = require('rethinkdb');

function defaultError(res, error) {
	res.status(500).json(error);
}

function defaultQuery(res, query) {
	Data.query(query)
		.then(function(results) {
			res.status(200).json(results);
		})
		.catch(function(error) {
			defaultError(res, error);
		});
}

function coerce(param) {

	if (/\|/.test(param)) {
		var args = param.split('|');

		switch (args[1]) {
			case 'bool':
				if (args[0] === 'true') {
					param = true;
				} else {
					param = false;
				}
				break;
			case 'int':
				param = parseInt(param, 10);
				break;
			case 'float':
				param = parseFloat(param);
				break;
		}

	}

	return param;

}

module.exports = function(app, router) {

	router.route('/:table')
		.get(function(req, res) {

			var query = r.table(req.params.table);

			defaultQuery(res, query);

		});

	router.route('/:table/:id')
		.get(function(req, res) {

			var query = r.table(req.params.table).get(req.params.id);

			defaultQuery(res, query);

		});

	router.route('/:table/:secondary/:tertiary')
		.get(function(req, res) {

			var query = r.table(req.params.table);

			var op = req.params.tertiary.substr(0,1);
			var value = coerce(req.params.tertiary.substr(1));

			if (config.id_regex.test(req.params.secondary)) {
				query = query.get(req.params.secondary).pluck(req.params.tertiary);
			} else if (op === '=') {

				var filter = {};
				filter[req.params.secondary] = value;

				query = query.filter(filter);

			} else if (op === '~') {

				query = query.filter(function(item) {
					return item(req.params.secondary).match(value);
				});

			}

			defaultQuery(res, query);

		});

};