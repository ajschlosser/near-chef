var _ = require('lodash');
var async = require('async');
var config = require('config').data;
var fs = require('fs');
var r = require('rethinkdb');

var conn;

function build_db() {

	r.connect()
		.then(function(connection) {
			conn = connection;
			return r.dbList().run(conn);
		})
		.then(function(result) {
			if (!_.includes(result, config.db)) {
				return r.dbCreate(config.db).run(conn);
			} else {
				return new Promise(function(resolve, reject) {
					resolve(result);
				});
			}
		})
		.then(function(result) {
			var tables = config.tables && config.tables.length > 0 ? config.tables : process.env.TABLES.split(',');
			if (tables && !_.isArray(tables)) {
				tables = [tables];
			}

			r.db(config.db).tableList().run(conn)
				.then(function(results) {
					_.each(tables, function(table, i) {
						tables[i] = {
							name  : table,
							built : _.includes(results, table) ? true : false
						};
					});

					async.each(tables, function(table, next) {

						function importData() {

							return new Promise(function(resolve, reject) {
								console.log('Attempting to import %s data', table.name);
								var path = './data/' + table.name + '.json';
								try {
									var data = require(path);
									console.log('- %s data exists, importing', table.name);
									r.db(config.db).table(table.name).delete().run(conn)
										.then(function(results) {
											return r.db(config.db).table(table.name).insert(data).run(conn);
										})
										.then(function(results) {
											resolve(results);
										});
								} catch(e) {
									console.log('- %s data does not exist: %s', table, e);
									resolve(false);
								}
							});

						}

						if (table.built === false) {
							console.log('Table %s does not exist, building', table.name);
							r.db(config.db).tableCreate(table.name).run(conn)
								.then(function(result) {
									return importData();
								})
								.then(function(imported){
									next();
								});
						} else {
							console.log('Table %s exists, importing data', table.name);
							importData()
								.then(function(imported){
									next();
								})
								.catch(function(err) {
									console.log(err);
								});
						}

					}, function(err) {

						if (err) {
							console.log(err);
						} else {
							console.log('Database built');
							process.exit(0);
						}

					});

				});

		});

};

build_db();