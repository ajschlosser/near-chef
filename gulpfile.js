var async	= require('async'),
	concat	= require('gulp-concat'),
	config	= require('config').gulp;
	del     = require('del'),
	es		= require('event-stream'),
	gulp	= require('gulp'),
	jshint	= require('gulp-jshint'),
	less	= require('gulp-less'),
	rem		= require('gulp-remove-code'),
	stylish	= require('jshint-stylish'),
	wrap	= require('gulp-wrap'),

gulp.task('vendors', function(){
	var sources = [],
		maps = [],
		path = config.vendors.src;
	if (!config.vendors.sources.length) {
		return
	}
	console.info('Processing...');
	del([config.vendors.dist]).then(function(paths) {
		async.parallel({
			vendors: function(completed) {
				async.each(config.vendors.sources, function(source, done) {
					console.info('\t...%s', source);
					sources.push(path + source);
					done();
				}, function(err){
					if (err) {
						console.error(err);
					} else {
						completed();
					}
				});
			},
			maps: function(completed) {
				async.each(config.vendors.maps, function(map, done) {
					console.info('\t...%s', map);
					maps.push(path + map);
					done();
				}, function(err){
					if (err) {
						console.error(err);
					} else {
						completed();
					}
				});
			}
		}, function(err){
			if (err) {
				console.error(err);
			} else {
				console.info('...Finished.');
				return es.merge(
					gulp.src(sources).pipe(concat('vendors.js')),
					gulp.src(maps))
					.pipe(gulp.dest(config.vendors.dist))
			}
		});
	});

});

gulp.task('scripts', function(){
	var src = config.scripts.src;
	if (config.scripts.sources && config.scripts.sources.length > 0) {
		src = config.scripts.sources;
	}
	del([config.scripts.dist]).then(function(paths) {
		return gulp.src(src)
			.pipe(jshint())
			.pipe(jshint.reporter(stylish))
			.pipe(concat(config.scripts.name))
			.pipe(rem({ production: true }))
			.pipe(wrap('(function(){\n\n<%= contents %>\n\n})();'))
			.pipe(gulp.dest(config.scripts.dist));
	});
});

gulp.task('assets', function(){
	del([config.assets.dist]).then(function(paths) {
		return gulp.src(config.assets.sources)
			.pipe(gulp.dest(config.assets.dist));
	});
});

gulp.task('styles', function(){
	del([config.styles.dist]).then(function(paths) {
		return gulp.src(config.styles.src)
			.pipe(less({paths: config.styles.src}))
			.pipe(concat(config.styles.name))
			.pipe(gulp.dest(config.styles.dist));
	});
});

gulp.task('templates', function(){
	del([config.templates.dist]).then(function(paths) {
		return gulp.src(config.templates.src)
			.pipe(gulp.dest(config.templates.dist));
	});
});

gulp.task('default', ['vendors', 'assets'], function(){
	gulp.watch(config.templates.src, ['templates']);
	gulp.watch(config.styles.src, ['styles']);
	gulp.watch([config.scripts.src, '!gulpfile.js'], ['scripts']);
	setTimeout(function() {
		console.info('Watching for changes...');
	},200);
});

gulp.task('dist', ['vendors', 'scripts', 'assets', 'styles', 'templates']);