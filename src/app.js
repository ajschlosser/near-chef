var App = angular.module('myApp', ['ui.router']);

App.config(function ($stateProvider, $urlRouterProvider) {
	$stateProvider
	.state('home', {
		url: "/home",
		templateUrl: "home/home.html",
		controller: "HomeCtrl"
	});
});


App.controller('AppCtrl', function AppCtrl() {
	console.log('app');
});

App.controller('HomeCtrl', function HomeCtrl($scope, _DataAPI) {

	var API = new _DataAPI('/institutions');

	$scope.lookup = function HomeCtrl_lookup() {

		API.get({
			id: $scope.ID
		}).then(function(response) {
			console.log(response.data);
			$scope.result = response.data;
		});

	};

	console.log('hi');
});