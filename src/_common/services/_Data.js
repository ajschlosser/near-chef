App.factory('_DataAPI', function($http, $q) {

	var DataAPI = function _DataAPI(endpoint) {
		var self = this;

		var api_path = '/api';

		return {

			get : function DataAPI_get(params) {

				params = params || {};
				var query = params.id || params.secondary + '/' + params.tertiary;

				var url = api_path + endpoint + '/' + query;

				return $http({
					method: 'GET',
					url: url
				});

			}

		};

	};

	return DataAPI;

});