var config = require('config').api;
var express = require('express');
var fs = require('fs');

var app = express();
app.use('/', express.static(__dirname + '/public'));
app.use('/angular', express.static(__dirname + '/public/angular'));
app.use('/vendor', express.static(__dirname + '/public/vendor'));

var router = express.Router();

fs.readdirSync(__dirname + '/routes/api').forEach(function(file) {
	if ((/.js$/).test(file)) {
		require('./routes/api/' + file)(app, router);
	}
});

app.use('/api', router);

var server = app.listen(process.env.PORT || config.port, function() {
	console.log('Serving running on port 3000');
});