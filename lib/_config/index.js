module.exports = {
	"data" : {
		"host"     : "localhost",
		"port"     : 28015,
		"db"       : "test",
		"tables"   : ['test'],
		"timeout"  : 20,
		"ssl"      : null
	},
	"api" : {
		"port"     : 3000,
		"id_regex" : /^\d+$/
	},
	"gulp" : {
		"templates": {
			"src": "src/**/*.html",
			"dist": "public"
		},
		"styles": {
			"src": "src/styles/**/*.less",
			"dist": "public/styles",
			"name": "styles.css"
		},
		"assets": {
			"dist": "public/assets",
			"sources": [
				"src/_assets/**/*",
				"!src/_assets/vendors"
			]
		},
		"scripts": {
			"src": "src/**/*.js",
			"dist": "public/js",
			"sources": [
				"src/**/*.js",
				"!src/_assets/vendors/**/*"
			],
			"name": "app.js"
		},
		"vendors": {
			"src": "src/_assets/vendors",
			"dist": "public/vendors",
			"sources": [],
			"maps": []
		}
	}
};