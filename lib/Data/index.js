var config = require('config').data;
var r = require('rethinkdb');
var _ = require('lodash');

function defaultError(error) {
	throw new Error(error);
}

module.exports = {
	_API: function Data__API() {
		console.log(__filename);
	},
	test: function Data_test() {
		return r.connect(config)
			.then(function(conn) {
				return r.dbList().run(conn);
			})
			.error(defaultError);
	},
	query: function Data_query(query) {
		return new Promise(function(resolve, reject) {
			r.connect(config)
				.then(function(conn) {
					return query.run(conn);
				})
				.then(function(cursor) {
					if (cursor && _.isFunction(cursor.toArray)) {
						return cursor.toArray();
					} else {
						return cursor;
					}
				})
				.then(function(results){
					resolve(results);
				})
				.error(defaultError);
		});
	}
};